package com.example.raghavkishan.wealthmanagement;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalDashboard extends Fragment {

    View personalDashboardLayoutView;

    FloatingActionButton addIncomeExpenseFloatingActionButton,addIncomeFloatingActionButton,addExpenseFloatingActionButton,sortPersonalDashboardFloatingActionButton;

    Animation addIncomeExpenseFloatingActionButtonOpen,addIncomeExpenseFloatingActionButtonClose,addIncomeExpenseFloatingActionButtonClockWise,addIncomeExpenseFloatingActionButtonAntiClockWise;

    boolean isOpen=false;

    RecyclerView personalDashboardRecyclerView;


    public PersonalDashboard() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        personalDashboardLayoutView =  inflater.inflate(R.layout.fragment_personal_dashboard, container, false);

        return personalDashboardLayoutView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        personalDashboardRecyclerView = (RecyclerView) personalDashboardLayoutView.findViewById(R.id.personalDashboardRecyclerView);


        addIncomeExpenseFloatingActionButton = (FloatingActionButton) personalDashboardLayoutView.findViewById(R.id.addIncomeExpenseFloatingActionButton);
        addIncomeFloatingActionButton = (FloatingActionButton) personalDashboardLayoutView.findViewById(R.id.addIncomeFloatingActionButton);
        addExpenseFloatingActionButton = (FloatingActionButton) personalDashboardLayoutView.findViewById(R.id.addExpenseFloatingActionButton);
        sortPersonalDashboardFloatingActionButton = (FloatingActionButton) personalDashboardLayoutView.findViewById(R.id.sortPersonalDashboardFloatingActionButton);

        addIncomeExpenseFloatingActionButtonOpen = AnimationUtils.loadAnimation(getActivity(),R.anim.open_income_expense);
        addIncomeExpenseFloatingActionButtonClose = AnimationUtils.loadAnimation(getActivity(),R.anim.close_income_expense);
        addIncomeExpenseFloatingActionButtonClockWise = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate_clockwise_45);
        addIncomeExpenseFloatingActionButtonAntiClockWise = AnimationUtils.loadAnimation(getActivity(),R.anim.rotate_anticlockwise_45);

        addIncomeExpenseFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isOpen){
                    addExpenseFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonClose);
                    addIncomeFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonClose);
                    sortPersonalDashboardFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonClose);
                    addIncomeExpenseFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonAntiClockWise);
                    addExpenseFloatingActionButton.setClickable(false);
                    addIncomeFloatingActionButton.setClickable(false);
                    sortPersonalDashboardFloatingActionButton.setClickable(false);
                    isOpen = false;
                }
                else
                {
                    addExpenseFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonOpen);
                    addIncomeFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonOpen);
                    sortPersonalDashboardFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonOpen);
                    addIncomeExpenseFloatingActionButton.startAnimation(addIncomeExpenseFloatingActionButtonClockWise);
                    addExpenseFloatingActionButton.setClickable(true);
                    addIncomeFloatingActionButton.setClickable(true);
                    sortPersonalDashboardFloatingActionButton.setClickable(true);
                    isOpen = true;
                }
            }
        });

        addIncomeFloatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                Log.d("rew","inside on click income button after alert dialogue builder");
                builder.setTitle("Select a form or input");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),android.R.layout.select_dialog_item);
                arrayAdapter.add("₹ / $");
                arrayAdapter.add("%");

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builder.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String selectedItem = arrayAdapter.getItem(which);
                        Log.d("rew"," the selected item in the adapter is: "+selectedItem.toString());
                        if(selectedItem == "₹ / $"){
                            Log.d("rew"," before replacing the screen with addIncomeValue Fragment");
                            FragmentManager fragments = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragments.beginTransaction();
                            AddIncomeValue addIncome_fragment = new AddIncomeValue();
                            fragmentTransaction.replace(R.id.mainactivity_frame_layout, addIncome_fragment);
                            fragmentTransaction.commit();
                        }
                        if(selectedItem == "%"){
                            FragmentManager fragments = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragments.beginTransaction();
                            AddIncomePercentage addIncomePercentage_fragment = new AddIncomePercentage();
                            fragmentTransaction.replace(R.id.mainactivity_frame_layout, addIncomePercentage_fragment);
                            fragmentTransaction.commit();
                        }

                    }
                });

                builder.show();
               /*FragmentManager fragments = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragments.beginTransaction();
                AddIncome addIncome_fragment = new AddIncome();
                fragmentTransaction.replace(R.id.mainactivity_frame_layout, addIncome_fragment);
                fragmentTransaction.commit();*/
            }
        });


    }
}
