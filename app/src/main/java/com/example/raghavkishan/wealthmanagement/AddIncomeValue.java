package com.example.raghavkishan.wealthmanagement;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddIncomeValue extends Fragment {

    View addIncomeValueLayoutInflaterView;

    Spinner incomeMainCategorySpinner,incomeCategoryTagsSpinner;

    EditText incomeEditText,incomeDescriptionEditText;

    Button addIncomeButton;

    ArrayList<String> incomeMainCategorySpinnerList = new ArrayList<String>();
    ArrayList<String> incomeCategoryTagsSpinnerList = new ArrayList<String>();

    String selectedIncomeMainCategory,selectedIncomeCategoryTag,enteredIncomeDescriptionText,firebaseGroupId,firebaseUserUid,firebaseGroupName;

    double enteredIncomeValue;

    boolean canPostIncome=true,groupExist=true;

    FirebaseAuth firebaseAuth;

    FirebaseUser user;

    FirebaseDatabase database;


    public AddIncomeValue() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        addIncomeValueLayoutInflaterView =  inflater.inflate(R.layout.fragment_add_income_value, container, false);

        return addIncomeValueLayoutInflaterView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Log.d("rew","inside on addIncomeValue Fragment");
        incomeMainCategorySpinner = (Spinner) addIncomeValueLayoutInflaterView.findViewById(R.id.incomeMainCategorySpinner);
        incomeCategoryTagsSpinner = (Spinner) addIncomeValueLayoutInflaterView.findViewById(R.id.incomeCategoryTagsSpinner);
        incomeEditText = (EditText) addIncomeValueLayoutInflaterView.findViewById(R.id.incomeEditText);
        incomeDescriptionEditText = (EditText) addIncomeValueLayoutInflaterView.findViewById(R.id.incomeDescriptionEditText);
        addIncomeButton = (Button) addIncomeValueLayoutInflaterView.findViewById(R.id.addIncomeButton);

        incomeMainCategorySpinnerMethod();

        database =  FirebaseDatabase.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        firebaseUserUid = user.getUid();

        Query userTable = database.getReference("users/"+firebaseUserUid);
        userTable.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                firebaseGroupId = dataSnapshot.child("groupId").getValue().toString();
                firebaseGroupName = dataSnapshot.child("groupName").getValue().toString();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        addIncomeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addIncome();
            }
        });


    }

    private void incomeMainCategorySpinnerMethod(){

        incomeMainCategorySpinnerList.add("Select Main Category");
        incomeMainCategorySpinnerList.add("Long Term Investment");
        incomeMainCategorySpinnerList.add("Medium Term Investment");
        incomeMainCategorySpinnerList.add("Short Term Investment");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_dropdown_item,incomeMainCategorySpinnerList);
        incomeMainCategorySpinner.setAdapter(adapter);
        incomeMainCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIncomeMainCategory = parent.getItemAtPosition(position).toString();
                incomeCategoryTagsSpinnerMethod(selectedIncomeMainCategory);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void incomeCategoryTagsSpinnerMethod(String selectIncomeMainCategory){

        if(selectIncomeMainCategory.equalsIgnoreCase("Select Main Category")){
            incomeCategoryTagsSpinnerList.clear();
        }

        if (selectIncomeMainCategory.equalsIgnoreCase("Long Term Investment")){
            incomeCategoryTagsSpinnerList.clear();
            incomeCategoryTagsSpinnerList.add("Retirement Fund");
            incomeCategoryTagsSpinnerList.add("Children Education Fund");
            incomeCategoryTagsSpinnerList.add("Real Estate");
        }

        if (selectIncomeMainCategory.equalsIgnoreCase("Medium Term Investment")){
            incomeCategoryTagsSpinnerList.clear();
            incomeCategoryTagsSpinnerList.add("Necessary");
            incomeCategoryTagsSpinnerList.add("Comforts");
            incomeCategoryTagsSpinnerList.add("Luxury");
        }

        if (selectIncomeMainCategory.equalsIgnoreCase("Short Term Investment")){
            incomeCategoryTagsSpinnerList.clear();
            incomeCategoryTagsSpinnerList.add("Necessary");
            incomeCategoryTagsSpinnerList.add("Comforts");
            incomeCategoryTagsSpinnerList.add("Luxury");
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_spinner_dropdown_item,incomeCategoryTagsSpinnerList);
        incomeCategoryTagsSpinner.setAdapter(adapter);
        incomeCategoryTagsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedIncomeCategoryTag = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void addIncome(){

        if (incomeEditText.getText().toString().matches("")){
            Toast.makeText(getActivity(), "Please complete all fields", Toast.LENGTH_SHORT).show();
        }else {

            enteredIncomeValue = Double.parseDouble(incomeEditText.getText().toString());
            enteredIncomeDescriptionText = incomeDescriptionEditText.getText().toString();

            if (enteredIncomeDescriptionText.equalsIgnoreCase("") || selectedIncomeMainCategory.equalsIgnoreCase("Select Main Category") || selectedIncomeCategoryTag.matches("")) {
                canPostIncome = false;
            } else {
                canPostIncome = true;
            }

            if (firebaseGroupId.equalsIgnoreCase("empty")) {
                groupExist = false;
            } else {
                groupExist = true;
            }

            if ((canPostIncome) && (groupExist)) {
                switch (selectedIncomeMainCategory) {
                    case "Long Term Investment":
                        Log.d("rew", "" + selectedIncomeCategoryTag);
                        switch (selectedIncomeCategoryTag) {
                            case "Retirement Fund":
                                longTermIncomePushMethod("retirementfund", firebaseUserUid, enteredIncomeDescriptionText);
                                break;
                            case "Children Education Fund":
                                longTermIncomePushMethod("childreneducationfund", firebaseUserUid, enteredIncomeDescriptionText);
                                break;
                            case "Real Estate":
                                longTermIncomePushMethod("realestate", firebaseUserUid, enteredIncomeDescriptionText);
                                break;
                        }
                        break;
                    case "Medium Term Investment":
                        switch (selectedIncomeCategoryTag) {
                            case "Necessary":
                                ShortMediumTermIncomePushMethod("necessary", "mediumterminvestment", firebaseUserUid);
                                break;
                            case "Comforts":
                                ShortMediumTermIncomePushMethod("comforts", "mediumterminvestment", firebaseUserUid);
                                break;
                            case "Luxury":
                                ShortMediumTermIncomePushMethod("luxury", "mediumterminvestment", firebaseUserUid);
                                break;
                        }
                        break;
                    case "Short Term Investment":
                        switch (selectedIncomeCategoryTag) {
                            case "Necessary":
                                ShortMediumTermIncomePushMethod("necessary", "shortterminvestment", firebaseUserUid);
                                break;
                            case "Comforts":
                                ShortMediumTermIncomePushMethod("comforts", "shortterminvestment", firebaseUserUid);
                                break;
                            case "Luxury":
                                ShortMediumTermIncomePushMethod("luxury", "shortterminvestment", firebaseUserUid);
                                break;
                        }
                }
            } else {
                if (!(canPostIncome)) {
                    Toast.makeText(getContext(), "Please complete all fields", Toast.LENGTH_SHORT).show();
                }
                if (!(groupExist)) {
                    Toast.makeText(getContext(), "Please create or join a group before you add income/expense", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void ShortMediumTermIncomePushMethod(String selectedIncomeCategoryTag,String selectedIncomeMainCategory,String firebaseUserUid){

        String path = selectedIncomeCategoryTag+"/"+selectedIncomeMainCategory;
        DatabaseReference node = database.getReference(path);
        String pushKey = node.push().getKey();
        IncomeNodeToPost incNode = new IncomeNodeToPost(firebaseUserUid,enteredIncomeValue,firebaseGroupId,enteredIncomeDescriptionText,firebaseGroupName);
        node.child(pushKey).setValue(incNode);
        Toast.makeText(getContext(), "Income Saved", Toast.LENGTH_SHORT).show();
        incomeEditText.setText("");
        incomeDescriptionEditText.setText("");
    }

    private void longTermIncomePushMethod(String path,String firebaseUserUid,String enteredIncomeDescriptionText){

        DatabaseReference node = database.getReference(path);
        String pushKey = node.push().getKey();
        IncomeNodeToPost incNode = new IncomeNodeToPost(firebaseUserUid,enteredIncomeValue,firebaseGroupId,enteredIncomeDescriptionText,firebaseGroupName);
        node.child(pushKey).setValue(incNode);
        Toast.makeText(getContext(), "Income Saved", Toast.LENGTH_SHORT).show();
        incomeEditText.setText("");
        incomeDescriptionEditText.setText("");
    }

}
