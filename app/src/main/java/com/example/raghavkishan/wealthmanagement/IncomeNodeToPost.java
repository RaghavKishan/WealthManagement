package com.example.raghavkishan.wealthmanagement;

/**
 * Created by raghavkishan on 6/29/2017.
 */

public class IncomeNodeToPost {



    public IncomeNodeToPost(String uid, double incomeValue, String groupId, String description, String groupName) {
        this.uid = uid;
        this.incomeValue = incomeValue;
        this.groupId = groupId;
        this.description = description;
        this.groupName = groupName;
    }



    public IncomeNodeToPost() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public double getIncomeValue() {
        return incomeValue;
    }

    public void setIncomeValue(double incomeValue) {
        this.incomeValue = incomeValue;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    String uid;
    double incomeValue;
    String groupId;
    String description;
    String groupName;


}
